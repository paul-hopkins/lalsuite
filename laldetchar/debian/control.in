Source: laldetchar
Section: science
Priority: optional
Maintainer: Steffen Grunewald <steffen.grunewald@aei.mpg.de>
Uploaders: Adam Mercer <adam.mercer@ligo.org>, GitLab <gitlab@git.ligo.org>
Build-Depends: debhelper (>= 9),
  dh-python,
  help2man,
  libglib2.0-dev (>= 2.14),
  libgsl-dev | libgsl0-dev (>= 1.9),
  liboctave-dev,
  pkg-config,
  python-all-dev,
  swig (>= 3.0.7),
  lal-dev (>= @MIN_LAL_VERSION@~),
  lal-octave (>= @MIN_LAL_VERSION@~),
  python-lal (>= @MIN_LAL_VERSION@~),
  lalmetaio-dev (>= @MIN_LALMETAIO_VERSION@~),
  lalmetaio-octave (>= @MIN_LALMETAIO_VERSION@~),
  python-lalmetaio (>= @MIN_LALMETAIO_VERSION@~),
  lalsimulation-dev (>= @MIN_LALSIMULATION_VERSION@~),
  lalsimulation-octave (>= @MIN_LALSIMULATION_VERSION@~),
  python-lalsimulation (>= @MIN_LALSIMULATION_VERSION@~),
  lalburst-dev (>= @MIN_LALBURST_VERSION@~),
  lalburst-octave (>= @MIN_LALBURST_VERSION@~),
  python-lalburst (>= @MIN_LALBURST_VERSION@~)
X-Python-Version: >= 2.7
X-Python3-Version: <= 3.0
Standards-Version: 3.9.8

Package: laldetchar
Architecture: any
Depends: ${misc:Depends},
  ${shlibs:Depends},
  libglib2.0-0,
  lal (>= @MIN_LAL_VERSION@~),
  lalmetaio (>= @MIN_LALMETAIO_VERSION@~),
  lalsimulation (>= @MIN_LALSIMULATION_VERSION@~),
  lalburst (>= @MIN_LALBURST_VERSION@~)
Description: LSC Algorithm Library Detector Characterisation
 The LSC Algorithm Detector Characterisation Library for gravitational
 wave data analysis. This package contains the shared-object libraries
 needed to run applications that use the LALDetChar library.

Package: laldetchar-dev
Section: libdevel
Architecture: any
Depends: ${misc:Depends},
  ${shlibs:Depends},
  libglib2.0-dev (>= 2.14),
  libgsl-dev | libgsl0-dev (>= 1.9),
  lal-dev (>= @MIN_LAL_VERSION@~),
  lalmetaio-dev (>= @MIN_LALMETAIO_VERSION@~),
  lalsimulation-dev (>= @MIN_LALSIMULATION_VERSION@~),
  lalburst-dev (>= @MIN_LALBURST_VERSION@~),
  laldetchar (= ${binary:Version})
Description: LSC Algorithm Library Detector Characterisation Developers
 The LSC Algorithm Detector Characterisation Library for gravitational
 wave data analysis. This package contains files needed build
 applications that use the LALDetChar library.

Package: laldetchar-python
Depends: python-laldetchar, ${misc:Depends}
Architecture: all
Priority: optional
Section: oldlibs
Description: transitional package
 laldetchar-python was renamed python-laldetchar,
 this is a transitional package, it can safely be removed.

Package: python-laldetchar
Replaces: laldetchar-python (<< 0.3.5-1~)
Breaks: laldetchar-python (<< 0.3.5-1~)
Section: python
Architecture: any
Depends: ${misc:Depends},
  ${python:Depends},
  ${shlibs:Depends},
  python-lal (>= @MIN_LAL_VERSION@~),
  python-lalmetaio (>= @MIN_LALMETAIO_VERSION@~),
  python-lalsimulation (>= @MIN_LALSIMULATION_VERSION@~),
  python-lalburst (>= @MIN_LALBURST_VERSION@~),
  laldetchar (= ${binary:Version})
Description: Python bindings for LALDetChar
 The LSC Algorithm Detchar Library for gravitational wave data analysis.
 This package contains Python bindings for the LAL Detchar library.

Package: laldetchar-octave
Architecture: any
Depends: ${misc:Depends},
  ${shlibs:Depends},
  octave,
  lal-octave (>= @MIN_LAL_VERSION@~),
  python-lalmetaio (>= @MIN_LALMETAIO_VERSION@~),
  lalsimulation-octave (>= @MIN_LALSIMULATION_VERSION@~),
  python-lalburst (>= @MIN_LALBURST_VERSION@~),
  laldetchar (= ${binary:Version})
Description: Octave bindings for LALDetChar
 The LSC Algorithm Detchar Library for gravitational wave data analysis.
 This package contains Octave bindings for the LAL Detchar library.
