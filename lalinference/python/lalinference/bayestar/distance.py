from .. import distance
from ..distance import *
__all__ = distance.__all__
from lalinference.bayestar.deprecation import warn
warn('ligo.skymap.distance')
