image: docker:latest

variables:
  DOCKER_DRIVER: overlay
#  GIT_DEPTH: 1
  BRANCH: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_NAME
  COMMIT: $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA
  NIGHTLY: $CI_REGISTRY_IMAGE:nightly
  TAG: $CI_REGISTRY_IMAGE:$CI_COMMIT_TAG
  LAL_DIR: $CI_PROJECT_DIR/opt/lalsuite

stages:
  - level0
  - level1
  - level2
  - level3
  - level4
  - docker
  - nightly
  - wheels
  - deploy

before_script:
  - ulimit -S -c 0
  - export VERBOSE="true"
  - export PATH=/usr/lib/ccache:/opt/local/libexec/ccache:$PATH
  - export CCACHE_DIR=${PWD}/ccache
  - export PKG_CONFIG_PATH=${LAL_DIR}/lib/pkgconfig
  - mkdir -p opt/lalsuite
  - if [ ${CI_PIPELINE_SOURCE} = "schedule" ] || [ ${CI_PIPELINE_SOURCE} = "web" ]; then EXTRA_CONFIG_FLAGS="--enable-nightly"; fi

cache:
  key: $CI_JOB_NAME
  paths:
    - ccache

.levelN:package: &levelN-package
  script:
    - cd ${CI_JOB_NAME#level?:*:}
    - . /etc/profile.d/modules.sh
    - module load mpi/openmpi-x86_64
    - ./00boot
    - ./configure --enable-swig --enable-doxygen --enable-mpi --prefix=${LAL_DIR}
    - make dist
    - cd ${CI_PROJECT_DIR}
    - tar xf ${CI_JOB_NAME#level?:*:}/lal*.tar.xz
    - cd lal*-*
    - mkdir -p subdir
    - cd subdir
    - ../configure --enable-swig --enable-doxygen --enable-mpi --prefix=${LAL_DIR}
    - make -j4 V=1
    - make -j4 V=1 VERBOSE=1 check
    - make -j4 install
    - make -j4 install-html
  artifacts:
    expire_in: 18h
    paths:
      - opt/lalsuite
  only:
    - pushes
  except:
    - tags

.levelN:package:stretch: &levelN-package-stretch
  image: ligo/lalsuite-dev:stretch
  <<: *levelN-package

level0:stretch:lal:
  <<: *levelN-package-stretch
  stage: level0

level1:stretch:lalframe:
  <<: *levelN-package-stretch
  stage: level1
  dependencies:
    - level0:stretch:lal

level1:stretch:lalmetaio:
  <<: *levelN-package-stretch
  stage: level1
  dependencies:
    - level0:stretch:lal

level1:stretch:lalsimulation:
  <<: *levelN-package-stretch
  stage: level1
  dependencies:
    - level0:stretch:lal

level1:stretch:lalxml:
  <<: *levelN-package-stretch
  stage: level1
  dependencies:
    - level0:stretch:lal

level2:stretch:lalburst:
  <<: *levelN-package-stretch
  stage: level2
  dependencies:
    - level1:stretch:lalmetaio
    - level1:stretch:lalsimulation

level2:stretch:lalinspiral:
  <<: *levelN-package-stretch
  stage: level2
  dependencies:
    - level1:stretch:lalframe
    - level1:stretch:lalmetaio
    - level1:stretch:lalsimulation

level2:stretch:lalpulsar:
  <<: *levelN-package-stretch
  stage: level2
  dependencies:
    - level0:stretch:lal

level2:stretch:lalstochastic:
  <<: *levelN-package-stretch
  stage: level2
  dependencies:
    - level1:stretch:lalmetaio

level3:stretch:laldetchar:
  <<: *levelN-package-stretch
  stage: level3
  dependencies:
    - level2:stretch:lalburst

level3:stretch:lalinference:
  <<: *levelN-package-stretch
  stage: level3
  dependencies:
    - level2:stretch:lalburst
    - level2:stretch:lalinspiral
    - level2:stretch:lalpulsar

level4:stretch:lalapps:
  <<: *levelN-package-stretch
  stage: level4
  dependencies:
    - level3:stretch:lalinference

.levelN:package:el7: &levelN-package-el7
  image: ligo/lalsuite-dev:el7
  <<: *levelN-package

level0:el7:lal:
  <<: *levelN-package-el7
  stage: level0

level1:el7:lalframe:
  <<: *levelN-package-el7
  stage: level1
  dependencies:
    - level0:el7:lal

level1:el7:lalmetaio:
  <<: *levelN-package-el7
  stage: level1
  dependencies:
    - level0:el7:lal

level1:el7:lalsimulation:
  <<: *levelN-package-el7
  stage: level1
  dependencies:
    - level0:el7:lal

level1:el7:lalxml:
  <<: *levelN-package-el7
  stage: level1
  dependencies:
    - level0:el7:lal

level2:el7:lalburst:
  <<: *levelN-package-el7
  stage: level2
  dependencies:
    - level1:el7:lalmetaio
    - level1:el7:lalsimulation

level2:el7:lalinspiral:
  <<: *levelN-package-el7
  stage: level2
  dependencies:
    - level1:el7:lalframe
    - level1:el7:lalmetaio
    - level1:el7:lalsimulation

level2:el7:lalpulsar:
  <<: *levelN-package-el7
  stage: level2
  dependencies:
    - level0:el7:lal

level2:el7:lalstochastic:
  <<: *levelN-package-el7
  stage: level2
  dependencies:
    - level1:el7:lalmetaio

level3:el7:laldetchar:
  <<: *levelN-package-el7
  stage: level3
  dependencies:
    - level2:el7:lalburst

level3:el7:lalinference:
  <<: *levelN-package-el7
  stage: level3
  dependencies:
    - level2:el7:lalburst
    - level2:el7:lalinspiral
    - level2:el7:lalpulsar

level4:el7:lalapps:
  <<: *levelN-package-el7
  stage: level4
  dependencies:
    - level3:el7:lalinference

nightly:top-level:jessie:
  image: ligo/lalsuite-dev:jessie
  stage: nightly
  script:
    - ./00boot
    - ./configure --enable-swig --enable-doxygen
    - make -j4 distcheck
  dependencies: []
  only:
    - schedules
    - web

nightly:top-level:el7-cr:
  image: ligo/lalsuite-dev:el7-cr
  stage: nightly
  script:
    - ./00boot
    - ./configure --enable-swig --enable-doxygen
    - make -j4 distcheck
  dependencies: []
  only:
    - schedules
    - web

nightly:top-level:sl7:
  image: ligo/lalsuite-dev:sl7
  stage: nightly
  script:
    - ./00boot
    - ./configure --enable-swig --enable-doxygen
    - make -j4 distcheck
  dependencies: []
  only:
    - schedules
    - web

nightly:top-level:trusty:
  image: skymoo/ligo-lalsuite-dev:trusty
  stage: nightly
  script:
    - ./00boot
    - ./configure --disable-swig --enable-doxygen
    - make -j4 distcheck
  dependencies: []
  only:
    - schedules
    - web

nightly:top-level:xenial:
  image: skymoo/ligo-lalsuite-dev:xenial
  stage: nightly
  script:
    - ./00boot
    - ./configure --disable-swig --enable-doxygen
    - make -j4 distcheck
  dependencies: []
  only:
    - schedules
    - web

nightly:top-level:bionic:
  image: skymoo/ligo-lalsuite-dev:bionic
  stage: nightly
  script:
    - ./00boot
    - ./configure --enable-doxygen
    - make -j4 distcheck
  dependencies: []
  only:
    - schedules
    - web

nightly:top-level:cosmic:
  image: skymoo/ligo-lalsuite-dev:cosmic
  stage: nightly
  script:
    - ./00boot
    - ./configure --enable-doxygen
    - make -j4 distcheck
  dependencies: []
  only:
    - schedules
    - web

# FIXME: It seems that Octave on buster currently has a few bugs which
# cause the Octave bindings to fail, disable building the Octave SWIG
# bindings for the time being.
nightly:top-level:buster:
  image: skymoo/ligo-lalsuite-dev:buster
  stage: nightly
  dependencies: []
  script:
    - ./00boot
    - ./configure --enable-doxygen
    - make -j4 distcheck
  only:
    - schedules
    - web

nightly:top-level:clang:
  image: skymoo/ligo-lalsuite-clang:jessie
  stage: nightly
  script:
    - ./00boot
    - CC=clang CXX=clang++ ./configure --enable-swig --enable-doxygen
    - make -j4 distcheck
  dependencies: []
  only:
    - schedules
    - web

nightly:openmp:stretch:
  image: ligo/lalsuite-dev:stretch
  stage: nightly
  dependencies: []
  script:
    - ./00boot
    - ./configure --enable-swig --enable-doxygen --enable-openmp
    - make -j4 distcheck
  only:
    - schedules
    - web

nightly:python3:stretch:
  image: ligo/lalsuite-dev:stretch
  stage: nightly
  script:
    - ./00boot
    - PYTHON=python3 ./configure --enable-swig --enable-doxygen
    - make -j4 distcheck
  dependencies: []
  only:
    - schedules
    - web

nightly:macos:highsierra:
  tags:
    - macos_highsierra
  stage: nightly
  script:
    - ./00boot
    # FIXME: disable building of Octave SWIG bindings until SWIG-4.0.0
    # is available
    - PYTHON=/opt/local/bin/python2.7 ./configure --enable-doxygen
    - make -j4
    - make -j4 check
  dependencies: []
  only:
    - schedules
    - web

nightly:macos:elcapitan:
  tags:
    - macos_elcapitan
  stage: nightly
  script:
    - ./00boot
    # FIXME: disable building of Octave SWIG bindings until SWIG-4.0.0
    # is available
    - PYTHON=/opt/local/bin/python2.7 ./configure --enable-doxygen
    - make -j4
    - make -j4 check
  dependencies: []
  only:
    - schedules
    - web

# Build receipe for standalone wheels on Linux
.nightly:wheel:manylinux1: &nightly-wheel-manylinux1
  # This container is derived from the official manylinux image provided by
  # python.org (see PEP 513), and includes all of the LALSuite
  # build-dependencies.
  image: containers.ligo.org/lscsoft/lalsuite-manylinux:master
  stage: wheels
  script:
    - PYPREFIX=/opt/python/$(echo ${CI_JOB_NAME} | sed 's/.*:\(.*\)-manylinux1/\1/')
    # Build wheel
    - ./00boot
    - ./configure PYTHON=${PYPREFIX}/bin/python --enable-mpi ${EXTRA_CONFIG_FLAGS}
    - make -j4 wheel
    # Bundle and fix up dependent shared libraries
    - auditwheel repair wheel/*.whl
    # Test
    - ${PYPREFIX}/bin/virtualenv test
    - source test/bin/activate
    - pip install wheelhouse/*
    - python -c 'import lal, lalframe, lalmetaio'
  dependencies: []
  only:
    - /^lalsuite-v.*$/
    - schedules
  artifacts:
    expire_in: 18h
    paths:
      - wheelhouse

# Build receipe for standalone wheels on macOS
.nightly:wheel:macos: &nightly-wheel-macos
  tags:
    - macos_elcapitan
  stage: wheels
  script:
    - PYVERS=$(echo ${CI_JOB_NAME} | sed 's/.*:cp\(.\)\(.\).*/\1.\2/')
    # Enter virtualenv so that we have a controlled version of Numpy
    - virtualenv-${PYVERS} env
    - source env/bin/activate
    - pip install git+https://github.com/lpsinger/delocate@fix-duplicate-libs glob2 'numpy==1.14.5;python_version>="3.7"' 'numpy==1.7.0;python_version<"3.7"'
    # Build wheel
    - ./00boot
    - ./configure PYTHON=$(which python${PYVERS}) --enable-mpi --enable-swig-python ${EXTRA_CONFIG_FLAGS} LDFLAGS=-Wl,-headerpad_max_install_names
    - make -j4 wheel
    # Bundle and fix up dependent shared libraries
    - delocate-wheel -v -w wheelhouse wheel/*.whl
    # Test
    - virtualenv-${PYVERS} test
    - source test/bin/activate
    - pip install wheelhouse/*
    - python -c 'import lal, lalframe, lalmetaio'
  dependencies: []
  only:
    - /^lalsuite-v.*$/
    - schedules
  artifacts:
    expire_in: 18h
    paths:
      - wheelhouse

# Build wheels for all supported platforms
nightly:wheel:cp27-cp27m-manylinux1:
  <<: *nightly-wheel-manylinux1
nightly:wheel:cp27-cp27mu-manylinux1:
  <<: *nightly-wheel-manylinux1
nightly:wheel:cp34-cp34m-manylinux1:
  <<: *nightly-wheel-manylinux1
nightly:wheel:cp35-cp35m-manylinux1:
  <<: *nightly-wheel-manylinux1
nightly:wheel:cp36-cp36m-manylinux1:
  <<: *nightly-wheel-manylinux1
nightly:wheel:cp37-cp37m-manylinux1:
  <<: *nightly-wheel-manylinux1
nightly:wheel:cp27-cp27m-macosx:
  <<: *nightly-wheel-macos
nightly:wheel:cp35-cp35m-macosx:
  <<: *nightly-wheel-macos
nightly:wheel:cp36-cp36m-macosx:
  <<: *nightly-wheel-macos
nightly:wheel:cp37-cp37m-macosx:
  <<: *nightly-wheel-macos

.levelN:rpm: &levelN-rpm-package
  image: ligo/lalsuite-dev:el7
  variables:
    RPM_BUILD_CPUS: 4
  script:
    - if [ -d rpmbuild ]; then yum -y install rpmbuild/RPMS/x86_64/*.rpm; fi
    - cd ${CI_JOB_NAME#level?:rpm:}
    - ./00boot
    - ./configure --enable-swig ${EXTRA_CONFIG_FLAGS}
    - make dist
    - rpmbuild -tb --define "_topdir $CI_PROJECT_DIR/rpmbuild" ${CI_JOB_NAME#level?:rpm:}-*.tar.xz
  artifacts:
    expire_in: 18h
    paths:
      - rpmbuild/RPMS/x86_64/${CI_JOB_NAME#level?:rpm:}-*.rpm
      - rpmbuild/RPMS/x86_64/python2-${CI_JOB_NAME#level?:rpm:}-*.rpm
  only:
    - schedules
    - tags
    - web

level0:rpm:lal:
  <<: *levelN-rpm-package
  stage: level0

level1:rpm:lalframe:
  <<: *levelN-rpm-package
  stage: level1
  dependencies:
    - level0:rpm:lal

level1:rpm:lalmetaio:
  <<: *levelN-rpm-package
  stage: level1
  dependencies:
    - level0:rpm:lal

level1:rpm:lalsimulation:
  <<: *levelN-rpm-package
  stage: level1
  dependencies:
    - level0:rpm:lal

level1:rpm:lalxml:
  <<: *levelN-rpm-package
  stage: level1
  dependencies:
    - level0:rpm:lal

level2:rpm:lalburst:
  <<: *levelN-rpm-package
  stage: level2
  dependencies:
    - level0:rpm:lal
    - level1:rpm:lalmetaio
    - level1:rpm:lalsimulation

level2:rpm:lalinspiral:
  <<: *levelN-rpm-package
  stage: level2
  dependencies:
    - level0:rpm:lal
    - level1:rpm:lalframe
    - level1:rpm:lalmetaio
    - level1:rpm:lalsimulation

level2:rpm:lalpulsar:
  <<: *levelN-rpm-package
  stage: level2
  dependencies:
    - level0:rpm:lal

level2:rpm:lalstochastic:
  <<: *levelN-rpm-package
  stage: level2
  dependencies:
   - level0:rpm:lal
   - level1:rpm:lalmetaio

level3:rpm:laldetchar:
  <<: *levelN-rpm-package
  stage: level3
  dependencies:
    - level0:rpm:lal
    - level1:rpm:lalmetaio
    - level1:rpm:lalsimulation
    - level2:rpm:lalburst

level3:rpm:lalinference:
  <<: *levelN-rpm-package
  stage: level3
  dependencies:
    - level0:rpm:lal
    - level1:rpm:lalframe
    - level1:rpm:lalmetaio
    - level1:rpm:lalsimulation
    - level2:rpm:lalburst
    - level2:rpm:lalinspiral
    - level2:rpm:lalpulsar

level4:rpm:lalapps:
  <<: *levelN-rpm-package
  stage: level4
  dependencies:
    - level0:rpm:lal
    - level1:rpm:lalframe
    - level1:rpm:lalmetaio
    - level1:rpm:lalsimulation
    - level2:rpm:lalburst
    - level2:rpm:lalinspiral
    - level2:rpm:lalpulsar
    - level3:rpm:lalinference

# build a nightly container from the RPMs
docker:nightly:el7:
  stage: docker
  before_script: []
  script:
    # add RPMs to directory to pass to docker
    - mkdir rpms && mv rpmbuild/RPMS/x86_64/*.rpm rpms
    - rm -rf rpmbuild*
    # build container and push to registry
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
    - docker build --pull -t $CI_REGISTRY_IMAGE/nightly:el7 --file .Dockerfile-el7.gitlab-ci .
    - docker push $CI_REGISTRY_IMAGE/nightly:el7
  dependencies:
    - level0:rpm:lal
    - level1:rpm:lalframe
    - level1:rpm:lalmetaio
    - level1:rpm:lalsimulation
    - level2:rpm:lalburst
    - level2:rpm:lalinspiral
    - level2:rpm:lalpulsar
    - level3:rpm:lalinference
    - level4:rpm:lalapps
  only:
    - schedules

# build a tagged container from the RPMs
docker:tags:el7:
  stage: docker
  before_script: []
  script:
    # add RPMs to directory to pass to docker
    - mkdir rpms && mv rpmbuild/RPMS/x86_64/*.rpm rpms
    - rm -rf rpmbuild*
    # build container and push to registry
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
    - docker build --pull -t $CI_REGISTRY_IMAGE/$CI_COMMIT_TAG:el7 --file .Dockerfile-el7.gitlab-ci .
    - docker push $CI_REGISTRY_IMAGE/$CI_COMMIT_TAG:el7
  dependencies:
    - level0:rpm:lal
    - level1:rpm:lalframe
    - level1:rpm:lalmetaio
    - level1:rpm:lalsimulation
    - level2:rpm:lalburst
    - level2:rpm:lalinspiral
    - level2:rpm:lalpulsar
    - level3:rpm:lalinference
    - level4:rpm:lalapps
  only:
    - /^lalsuite-v.*$/

.levelN:deb: &levelN-deb-package
  image: ligo/lalsuite-dev:stretch
  script:
    # install debs from artifacts
    - dpkg -i *.deb || true
    # make dist tarball
    - cd ${CI_JOB_NAME#level?:deb:}
    - ./00boot
    - ./configure --enable-swig ${EXTRA_CONFIG_FLAGS}
    - make dist
    # create orig tarball
    - PACKAGE=$(echo ${CI_JOB_NAME} | sed 's/.*://')
    - TARBALL="${PACKAGE}-*.tar.*"
    - SUFFIX=$(echo $TARBALL | sed 's/.*\.\(tar\..*\)/\1/')
    - VERSION=$(echo $TARBALL | sed 's/[^-]*-\(.*\)\.tar\..*/\1/' | tr '-' '~')
    - cd ${CI_PROJECT_DIR}
    - cp ${CI_JOB_NAME#level?:deb:}/${TARBALL} ${PACKAGE}_${VERSION}.orig.${SUFFIX}
    # update changelog
    - export DEBFULLNAME="GitLab"
    - export DEBEMAIL="gitlab@git.ligo.org"
    - tar xf ${CI_JOB_NAME#level?:deb:}/${TARBALL}
    - cd ${PACKAGE}-*
    - dch -v ${VERSION}-1 -b 'Rebuilt automatically on git.ligo.org CI'
    # build packages
    - debuild -us -uc -r
  artifacts:
    expire_in: 18h
    paths:
      - ${CI_JOB_NAME#level?:deb:}*.changes
      - ${CI_JOB_NAME#level?:deb:}*.deb
      - python*-${CI_JOB_NAME#level?:deb:}*.deb
      - ${CI_JOB_NAME#level?:deb:}*.dsc
      - ${CI_JOB_NAME#level?:deb:}*.orig.*
  only:
    - schedules
    - tags
    - web

level0:deb:lal:
  <<: *levelN-deb-package
  stage: level0

level1:deb:lalframe:
  <<: *levelN-deb-package
  stage: level1
  dependencies:
    - level0:deb:lal

level1:deb:lalmetaio:
  <<: *levelN-deb-package
  stage: level1
  dependencies:
    - level0:deb:lal

level1:deb:lalsimulation:
  <<: *levelN-deb-package
  stage: level1
  dependencies:
    - level0:deb:lal

level1:deb:lalxml:
  <<: *levelN-deb-package
  stage: level1
  dependencies:
    - level0:deb:lal

level2:deb:lalburst:
  <<: *levelN-deb-package
  stage: level2
  dependencies:
    - level0:deb:lal
    - level1:deb:lalmetaio
    - level1:deb:lalsimulation

level2:deb:lalinspiral:
  <<: *levelN-deb-package
  stage: level2
  dependencies:
    - level0:deb:lal
    - level1:deb:lalframe
    - level1:deb:lalmetaio
    - level1:deb:lalsimulation

level2:deb:lalpulsar:
  <<: *levelN-deb-package
  stage: level2
  dependencies:
    - level0:deb:lal

level2:deb:lalstochastic:
  <<: *levelN-deb-package
  stage: level2
  dependencies:
   - level0:deb:lal
   - level1:deb:lalmetaio

level3:deb:laldetchar:
  <<: *levelN-deb-package
  stage: level3
  dependencies:
    - level0:deb:lal
    - level1:deb:lalmetaio
    - level1:deb:lalsimulation
    - level2:deb:lalburst

level3:deb:lalinference:
  <<: *levelN-deb-package
  stage: level3
  dependencies:
    - level0:deb:lal
    - level1:deb:lalframe
    - level1:deb:lalmetaio
    - level1:deb:lalsimulation
    - level2:deb:lalburst
    - level2:deb:lalinspiral
    - level2:deb:lalpulsar

level4:deb:lalapps:
  <<: *levelN-deb-package
  stage: level4
  dependencies:
    - level0:deb:lal
    - level1:deb:lalframe
    - level1:deb:lalmetaio
    - level1:deb:lalsimulation
    - level2:deb:lalburst
    - level2:deb:lalinspiral
    - level2:deb:lalpulsar
    - level3:deb:lalinference

# build a nightly container from the debian packages
docker:nightly:stretch:
  stage: docker
  before_script: []
  script:
    # add deb packages to directory to pass to docker
    - mkdir debs && mv *.deb debs
    - rm *.changes *.dsc *.orig.*
    # build container and push to registry
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
    - docker build --pull -t $CI_REGISTRY_IMAGE/nightly:stretch --file .Dockerfile-stretch.gitlab-ci .
    - docker push $CI_REGISTRY_IMAGE/nightly:stretch
  dependencies:
    - level0:deb:lal
    - level1:deb:lalframe
    - level1:deb:lalmetaio
    - level1:deb:lalsimulation
    - level2:deb:lalburst
    - level2:deb:lalinspiral
    - level2:deb:lalpulsar
    - level3:deb:lalinference
    - level4:deb:lalapps
  only:
    - schedules

# build a tagged container from the debian packages
docker:tags:stretch:
  stage: docker
  before_script: []
  script:
    # add deb packages to directory to pass to docker
    - mkdir debs && mv *.deb debs
    - rm *.changes *.dsc *.orig.*
    # build container and push to registry
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
    - docker build --pull -t $CI_REGISTRY_IMAGE/$CI_COMMIT_TAG:stretch --file .Dockerfile-stretch.gitlab-ci .
    - docker push $CI_REGISTRY_IMAGE/$CI_COMMIT_TAG:stretch
  dependencies:
    - level0:deb:lal
    - level1:deb:lalframe
    - level1:deb:lalmetaio
    - level1:deb:lalsimulation
    - level2:deb:lalburst
    - level2:deb:lalinspiral
    - level2:deb:lalpulsar
    - level3:deb:lalinference
    - level4:deb:lalapps
  only:
    - /^lalsuite-v.*$/

pages:
  image: ligo/lalsuite-dev:stretch
  stage: nightly
  script:
    - ./00boot
    - ./configure --enable-doxygen --prefix=${LAL_DIR}
    - make -j4 install-html
    - cp -r ${LAL_DIR}/share/doc public/
    - pushd public
    - cp lalsuite/index.html index.html
    - sed -i 's/..\/lal/lal/g' index.html
    - popd
  dependencies: []
  artifacts:
    paths:
      - public
  only:
    - master@lscsoft/lalsuite
  except:
    - pushes
    - web

deploy:wheel:
  stage: deploy
  image: containers.ligo.org/lscsoft/lalsuite-manylinux:master
  variables:
    GIT_STRATEGY: none
  script:
    # exit if we're not running in the main namespace
    - if [[ ${CI_PROJECT_PATH} != "lscsoft/lalsuite" ]]; then echo "Not deploying."; exit 0; fi
    # TWINE_USERNAME and TWINE_PASSWORD are provided by CI secret variables
    - /opt/python/cp36-cp36m/bin/pip install twine
    - /opt/python/cp36-cp36m/bin/twine upload wheelhouse/*
  dependencies:
    - nightly:wheel:cp27-cp27m-manylinux1
    - nightly:wheel:cp27-cp27mu-manylinux1
    - nightly:wheel:cp34-cp34m-manylinux1
    - nightly:wheel:cp35-cp35m-manylinux1
    - nightly:wheel:cp36-cp36m-manylinux1
    - nightly:wheel:cp37-cp37m-manylinux1
    - nightly:wheel:cp27-cp27m-macosx
    - nightly:wheel:cp35-cp35m-macosx
    - nightly:wheel:cp36-cp36m-macosx
    - nightly:wheel:cp37-cp37m-macosx
  only:
    - /^lalsuite-v.*$/
    - schedules
